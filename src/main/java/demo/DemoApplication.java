package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import demo.DacRepository;

@SpringBootApplication
@Configuration
public class DemoApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        final DacRepository dacRepository  = context.getBean(DacRepository.class);
        dacRepository.incrKeyExample();
    }
    
    
    @Bean
    public RedisTemplate<String, Long> getLongRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
    	 final RedisTemplate<String,Long> redisTemplate = new RedisTemplate<String, Long>();
    	 redisTemplate.setConnectionFactory(redisConnectionFactory);
    	 redisTemplate.setHashValueSerializer(new GenericToStringSerializer<Long>(Long.class));
    	 redisTemplate.setKeySerializer(new StringRedisSerializer());
    	 redisTemplate.setValueSerializer(new GenericToStringSerializer<Long>(Long.class));
    	 redisTemplate.setHashKeySerializer(new StringRedisSerializer());
    	 redisTemplate.afterPropertiesSet();
    	 return redisTemplate;
    }
}
