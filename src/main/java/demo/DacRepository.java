package demo;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DacRepository {

	private final HashOperations<String, String, Long> hashOps;
	
    @Autowired
	private final RedisTemplate<String, Long> redisTemplate;
    
	@Autowired
	public DacRepository(RedisTemplate<String, Long> redisTemplate) {
		this.redisTemplate = redisTemplate;
		hashOps = redisTemplate.opsForHash();

	}
	
    
	public void incrKeyExample() {
		final Set<String> keys = this.redisTemplate.keys("*");
		for(String key: keys) {
			System.out.println("key: "+ key);
		}
		
		final String key = "deal-1";
		final String field = "view";
		final Long value = 1L;
		hashOps.put(key, field, value);
		final Long delta = 1L;
		hashOps.increment(key, field, delta);
		Long val = hashOps.get("deal-1", "view");
		System.out.println("Value = "+val);
		
		List<String> fields = new ArrayList<String>();
		fields.add(field);
		List<Long> views = hashOps.multiGet(key, fields);
		System.out.println("Value = "+views);
	}

}
